class HistoriaMedico :
    def __init__(self, enfermedad, fechaEnfermo):
        self.enfermedad = enfermedad
        self.fechaEnfermo = fechaEnfermo


    def __str__(self):
        return f"Enfermedad: {self.enfermedad},Fecha de Enfermo: {self.fechaEnfermo}"
    def getenfermedad(self):
        return self.enfermedad
    def getfechaenfermo(self):
        return self.fechaEnfermo
    def setEnfermedad(self, Enfermedad):
        self.enfermedad = Enfermedad
    def setFechaEnfermo(self, FechaEnfermo):
        self.fechaEnfermo = FechaEnfermo


def crearHistoria():
    enfermedad = input("Ingrese la enfermedad de la Mascota: ")
    fechaEnfermo = input("Ingrese la fecha de Enfermo de la Mascota: ")
    historiaNueva = HistoriaMedico (enfermedad, fechaEnfermo)
    listaHistoria.append(historiaNueva)

def actualizarHistoria():
    mostrarHistoria()
    indice = int(input("Ingrese el numero de Historia que desea modificar: "))
    enfermedad = input("Ingrese la nueva enfermedad de la Mascota Cedula: ")
    listaHistoria[indice - 1].setEnfermedad(enfermedad)
    fechaEnfermo = input("Ingrese la nueva fecha de Enfermo de la Mascota: ")
    listaHistoria[indice - 1].setFechaEnfermo(fechaEnfermo)

def mostrarHistoria():
    print(" ")
    print("----Informe de Historia----")
    for indice in range(0, len(listaHistoria)):
        print(f"{indice +1} - {listaHistoria[indice]}")

def borrarHistoria():
    mostrarHistoria()
    indice = int(input("Ingrese el numero de historia que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaHistoria[indice -1].getenfermedad()} {listaHistoria[indice-1].getfechaEnfermo()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaHistoria.remove(listaHistoria[indice -1])


listaHistoria = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Historia Medica--------")
    print ("C - Crear Historia")
    print ("A - Actualizar Historia")
    print ("M - Mostrar Historia")
    print ("B - Borrar Historia")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'c'):
        crearHistoria()
    elif(opcion == 'a'):
        actualizarHistoria()
    elif(opcion == 'm'):
        mostrarHistoria()
    elif(opcion == 'b'):
        borrarHistoria()

    else :
            print("Opcion Incorrecta")
