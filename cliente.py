class Cliente :
    def __init__(self, cedula, codigoDeCliente, primerApellido, nombres, direccion, telefono):
        self.cedula = cedula
        self.codigoDeCliente = codigoDeCliente
        self.primerApellido = primerApellido
        self.nombres = nombres
        self.direccion = direccion
        self.telefono = telefono

    def __str__(self):
        return f"Nombre:  {self.nombres}, Apellido: {self.primerApellido} Codigo de Cliente: {self.codigoDeCliente},Direccion: {self.direccion} Telefono: {self.telefono}"
    def getnombres(self):
        return self.nombres
    def getprimerApellido(self):
        return self.primerApellido
    def setCedula(self, Cedula):
        self.cedula = Cedula
    def setCodigoDelCliente(self, CodigoDelCliente):
        self.codigoDelCliente = CodigoDelCliente
    def setNombres(self, Nombres):
        self.nombres = Nombres
    def setPrimerApellido(self, PrimerApellido):
        self.primerApellido = PrimerApellido
    def setDireccion(self, Direccion):
        self.direccion = Direccion
    def setTelefono(self, Telefono):
        self.telefono = Telefono


def crearCliente():
    cedula = input("Ingrese la Cedula: ")
    codigoDelCliente = input("Ingrese el Codigo del Cliente: ")
    primerApellido = input("Ingrese el Apellido del Cliente: ")
    nombres = input("Ingrese el Nombre del Cliente: ")
    direccion = input("Ingrese la Direcion del Cliente: ")
    telefono = input("Ingrese el Telefono: ")
    clienteNuevo = Cliente (nombres, codigoDelCliente, primerApellido, nombres, direccion, telefono)
    listaClientes.append(clienteNuevo)

def actualizarCliente():
    mostrarCliente()
    indice = int(input("Ingrese el numero de Cliente que desea modificar: "))
    cedula = input("Ingrese la nueva Cedula: ")
    listaClientes[indice - 1].setCedula(cedula)
    codigoDelCliente = input("Ingrese el nuevo Codigo del Cliente: ")
    listaClientes[indice - 1].setCodigoDelCliente(codigoDelCliente)
    primerApellido = input("Ingrese el nuevo Apellido del Cliente: ")
    listaClientes[indice - 1].setPrimerApellido(primerApellido)
    nombres = input("Ingrese el Nombre nuevo del Cliente: ")
    listaClientes[indice - 1].setNombres(nombres)
    telefono = input("Ingrese el nuevo Telefono del Cliente: ")
    listaClientes[indice - 1].setTelefono (telefono)
    direccion = input("Ingrese la nueva Direccion del Cliente: ")
    listaClientes[indice - 1].setDireccion (direccion)



def mostrarCliente():
    print(" ")
    print("----Informe de Clientes----")
    for indice in range(0, len(listaClientes)):
        print(f"{indice +1} - {listaClientes[indice]}")

def borrarCliente():
    mostrarCliente()
    indice = int(input("Ingrese el numero de cliente que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaClientes[indice -1].getprimerApellido()} {listaClientes[indice-1].getnombres()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaClientes.remove(listaClientes[indice -1])


listaClientes = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Clientes--------")
    print ("C - Crear Cliente")
    print ("A - Actualizar Cliente")
    print ("M - Mostrar Clientes")
    print ("B - Borrar Cliente")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'c'):
        crearCliente()
    elif(opcion == 'a'):
        actualizarCliente()
    elif(opcion == 'm'):
        mostrarCliente()
    elif(opcion == 'b'):
        borrarCliente()

    else :
            print("Opcion Incorrecta")
