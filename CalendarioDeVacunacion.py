class CalendarioVacunacion :
    def __init__(self, fechacadaVacuna, enfermedadqueseVacuno):
        self.fechacadaVacuna = fechacadaVacuna
        self.enfermedadqueseVacuno = enfermedadqueseVacuno


    def __str__(self):
        return f"Fecha de Cada Vacuna: {self.fechacadaVacuna} Enfermedad que se Vacuno: {self.enfermedadqueseVacuno}"
    def getfechacadaVacuna(self):
        return self.fechacadaVacuna
    def getenfermedadqueseVacuno(self):
        return self.enfermedadqueseVacuno
    def setfechacadaVacuna(self, FechacadaVacuna):
        self.fechacadaVacuna = FechacadaVacuna
    def setEnfermedadqueseVacuno(self, EnfermedadqueseVacuno):
        self.enfermedadqueseVacuno = EnfermedadqueseVacuno


def crearCalendario():
    fechacadaVacuna = input("Ingrese la fecha de Vacunacion de la Mascota: ")
    enfermedadquesevacuno = input("Ingrese la enfermedad que se vacuno la Mascota: ")
    calendarioNuevo = CalendarioVacunacion (fechacadaVacuna, enfermedadquesevacuno)
    listaCalendario.append(calendarioNuevo)

def actualizarCalendario():
    mostrarCalendario()
    indice = int(input("Ingrese el numero de Calendario que desea modificar: "))
    fechacadaVacuna = input("Ingrese la nueva fecha de vacunacion de la Mascota: ")
    listaCalendario[indice - 1].setFechacadaVacuna(fechacadaVacuna)
    enfermedadqueseVacuno = input("Ingrese la nueva enfermedad que se vacuno la Mascota: ")
    listaCalendario[indice - 1].setEnfermedadqueseVacuno(enfermedadqueseVacuno)

def mostrarCalendario():
    print(" ")
    print("----Informe de Calendario de Vacunacion----")
    for indice in range(0, len(listaCalendario)):
        print(f"{indice +1} - {listaCalendario[indice]}")

def borrarCalendario():
    mostrarCalendario()
    indice = int(input("Ingrese el numero de Calendario que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaCalendario[indice -1].getfechacadaVacuna()} {listaCalendario[indice-1].getenfermedadqueseVacuno()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaCalendario.remove(listaCalendario[indice -1])


listaCalendario = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Calendario de Vacunacion--------")
    print ("C - Crear Calendario")
    print ("A - Actualizar Calendario")
    print ("M - Mostrar Calendario")
    print ("B - Borrar Calendario")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'c'):
        crearCalendario()
    elif(opcion == 'a'):
        actualizarCalendario()
    elif(opcion == 'm'):
        mostrarCalendario()
    elif(opcion == 'b'):
        borrarCalendario()

    else :
            print("Opcion Incorrecta")
