class Mascotas :
    def __init__(self, codigo, alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual):
        self.codigo = codigo
        self.alias = alias
        self.especie = especie
        self.raza = raza
        self.colorPelo = colorPelo
        self.fechaNacimiento = fechaNacimiento
        self.pesoMedio = pesoMedio
        self.pesoActual = pesoActual

    def __str__(self):
        return f"Codigo:  {self.codigo}, Alias: {self.alias} Especie: {self.especie},Raza: {self.raza} ColorPelo: {self.colorPelo} Fecha de Nacimiento: {self.fechaNacimiento}  Peso Medio: {self.pesoMedio} Peso Actual: {self.pesoActual}"

    def getcodigo(self):
        return self.codigo
    def getalias(self):
        return self.alias
    def setCodigo(self, Codigo):
        self.codigo = Codigo
    def setAlias(self, Alias):
        self.alias = Alias
    def setColorPelo(self, ColorPelo):
        self.colorPelo = ColorPelo
    def setEspecie(self, Especie):
        self.especie = Especie
    def setRaza(self, Raza):
        self.raza = Raza
    def setFechaNacimiento(self, FechaNacimiento):
        self.fechaNacimiento = FechaNacimiento
    def setPesoMedio(self, PesoMedio):
        self.pesoMedio = PesoMedio
    def setPesoActual(self, PesoActual):
        self.pesoActual = PesoActual



def crearMascota():
    codigo = input("Ingrese el codigo de la Mascota: ")
    alias = input("Ingrese el Alias de la Mascota: ")
    especie = input("Ingrese la especie de la Mascota: ")
    raza = input("Ingrese la raza de la Mascota: ")
    colorPelo = input("Ingrese el color de Pelo de la Mascota: ")
    fechaNacimiento = input("Ingrese la fecha de Naciemiento de la Mascota: ")
    pesoMedio = input("Ingrese el peso medio de la Mascota: ")
    pesoActual = input("Ingrese el peso Actual de la Mascota: ")
    mascotaNueva = Mascotas (codigo, alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual)
    listaMascota.append(mascotaNueva)

def actualizarMascota():
    mostrarMascota()
    indice = int(input("Ingrese el numero de Mascota que desea modificar: "))
    codigo = input("Ingrese el nuevo codigo de Mascota: ")
    listaMascota[indice - 1].setCodigo(codigo)
    alias = input("Ingrese el nuevo Alias de la Mascota: ")
    listaMascota[indice - 1].setAlias(alias)
    especie = input("Ingrese la nueva especie de la Mascota: ")
    listaMascota[indice - 1].setEspecie(especie)
    raza = input("Ingrese la nueva raza de la mascota: ")
    listaMascota[indice - 1].setRaza(raza)
    colorPelo = input("Ingrese el nuevo color de Pelo de la Mascota: ")
    listaMascota[indice - 1].setColorPelo (colorPelo)
    fechaNacimiento = input("Ingrese la nueva fecha de Nacimiento de la Mascota: ")
    listaMascota[indice - 1].setFechaNacimiento (fechaNacimiento)
    pesoMedio = input("Ingrese el nuevo peso Medio de la Mascota: ")
    listaMascota[indice - 1].setPesoMedio (pesoMedio)
    pesoActual = input("Ingrese el nuevo peso Actual de la Mascota: ")
    listaMascota[indice - 1].setPesoActual (pesoActual)


def mostrarMascota():
    print(" ")
    print("----Informe de Mascotas----")
    for indice in range(0, len(listaMascota)):
        print(f"{indice +1} - {listaMascota[indice]}")

def borrarMascota():
    mostrarMascota()
    indice = int(input("Ingrese el numero de Mascota que desea borrar: "))
    print(f"Esta seguro que desea borrar a: {listaMascota[indice -1].getcodigo()} {listaMascota[indice-1].getalias()}")
    respuesta = input(" S - Borrar - N - No Borrar ")
    if (respuesta == "s"):
        listaMascota.remove(listaMascota[indice -1])


listaMascota = []
opcion = ' '
while(opcion != 'x'):
    print("-----Menu de Mascotas--------")
    print ("C - Crear Mascota")
    print ("A - Actualizar Mascota")
    print ("M - Mostrar Mascota")
    print ("B - Borrar Mascota")
    print ("X - Salir")
    opcion = input("Ingrese la Opcion deseada: ")
    if(opcion == 'x'):
            print ("Saliendo...")
    elif(opcion == 'c'):
        crearMascota()
    elif(opcion == 'a'):
        actualizarMascota()
    elif(opcion == 'm'):
        mostrarMascota()
    elif(opcion == 'b'):
        borrarMascota()

    else :
            print("Opcion Incorrecta")
